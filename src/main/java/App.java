import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.time.LocalDate;

public class App {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/university_sda10";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "Nokiac7*";
    private static Connection connection;

    public App() throws SQLException {
    }

    public static void main(String[] args) throws SQLException {
        connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
        getCourses();

        System.out.println("_____________________________________________________________________________");

        getProfesor();

        insertDepartment();
        updateCourse();

        updateCourse("Optional", "Fizica aplicata");

        printCourseByName("Fizica aplicata");

        saveStudentInTranzaction("Test tranzaction seccess", 1, 1);
        saveStudentInTranzaction("Test tranzaction failed",1,150);
        connection.close();
    }

    private static void getCourses() throws SQLException {

        String sql = "  select * from curs ";
        Statement statement = connection.createStatement();
        statement.executeQuery(sql);
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {


            int id = rs.getInt("id_curs");
            String name = rs.getString("nume");
            int credits = rs.getInt("credite");
            String description = rs.getString("descriere");
            int department_id = rs.getInt("id_departament");
            System.out.println(id + " " + name + " " + credits + " " + description + " " + department_id);
        }
        rs.close();
        statement.close();

    }

    private static void getProfesor() throws SQLException {
        String profesorSql = "  select p.id_profesor, p.nume, p.adresa, p.salariu, p.data_angajare, d.nume\n" +
                " from profesor p\n" +
                " join departament d \n" +
                " on p.id_departament = d.id_departament; ;";

        Statement statement2 = connection.createStatement();
        ResultSet resultSet = statement2.executeQuery(profesorSql);
        while (resultSet.next()) {
            String deparmentName = resultSet.getString("d.nume");
            int profesorId = resultSet.getInt("p.id_profesor");
            String profName = resultSet.getString("p.nume");
            String adress = resultSet.getString("p.adresa");
            int salary = resultSet.getInt("p.salariu");
            LocalDate hireDate = resultSet.getDate("p.data_angajare").toLocalDate();

            System.out.println(profesorId + " " + profName + " " + adress + " " + salary + " " + hireDate + " " + deparmentName);
        }

        resultSet.close();
        statement2.close();

    }

    private static void insertDepartment() throws SQLException {
        String sql = "insert into departament (nume) values ('Java')";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();

        System.out.println("-------------------------------------------------------------------");
    }

    private static void updateCourse() throws SQLException {
        String sql = " update curs set curs.descriere = 'Obligatoriu' where curs.nume = 'SQL'";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);

        statement.close();

    }

    private static void updateCourse(String description, String courseName) throws SQLException {
        String sql = " update curs set curs.descriere = ? where curs.nume = ? ";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, description);
        preparedStatement.setString(2, courseName);
        preparedStatement.executeUpdate();

        preparedStatement.close();
        System.out.println("----------------------------------------------------------------------------");
    }

    private static void printCourseByName(String courseName) throws SQLException {

        String sql = " select * from curs where nume= ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, courseName);
        //preparedStatement.setInt(1, id_course);
        ResultSet rs = preparedStatement.executeQuery();

        while (rs.next()) {
            int id = rs.getInt("id_curs");
            String name = rs.getString("nume");
            int credits = rs.getInt("credite");
            String description = rs.getString("descriere");
            int department_id = rs.getInt("id_departament");
            System.out.println(id + " " + name + " " + credits + " " + description + " " + department_id);
        }
        preparedStatement.close();
        rs.close();

    }

    private static void saveStudentInTranzaction(String name, int profId, int sectionId) throws SQLException {
        String studentSql = "insert into student (nume, id_profesor) values (?,?)";
        try {

            connection.setAutoCommit(false);

            PreparedStatement studentStatement = connection.prepareStatement(studentSql, Statement.RETURN_GENERATED_KEYS);
            studentStatement.setString(1, name);
            studentStatement.setInt(2, profId);

            studentStatement.executeUpdate();
            ResultSet rs = studentStatement.getGeneratedKeys();
            rs.next();
            int studentId = rs.getInt(1);

            studentStatement.close();

            String sectionSql = "insert into sectie_student(id_sectie, id_student) values (?,?)";
            PreparedStatement sectionStatement = connection.prepareStatement(sectionSql);
            sectionStatement.setInt(1, sectionId);
            sectionStatement.setInt(2, studentId);

            sectionStatement.executeUpdate();
            sectionStatement.close();

            connection.commit();
        } catch (SQLException e) {
            try {

                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
e.printStackTrace();
        }

    }
}